//
//  TBMainViewController.swift
//  TeamBuilder
//
//  Created by Alexey Zhizhensky on 1/31/20.
//  Copyright © 2020 Alexey Zhizhensky. All rights reserved.
//

import UIKit
import CoreData

class TBMainViewController: UIViewController {
    
    //MARK: - outlets
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            self.tableView.delegate = self
            self.tableView.dataSource = self
        }
    }
    
    //MARK: - variables
    
    let fetchRequest: NSFetchRequest<TBTeam> =
        CoreDataManager.sh.fetchRequest(entityName: "TBTeam", keysForSort: ["order", "name"])
    var teams: [TBTeam] = []
    override var preferredStatusBarStyle: UIStatusBarStyle {
            return .lightContent
    }
    
    //MARK: - gui variables
    
    private lazy var addBarButtonItem: UIBarButtonItem = {
        let item = UIBarButtonItem(barButtonSystemItem: .add,
                                   target: self,
                                   action: #selector(self.addButtonTapped))
        return item
    }()
    
    private lazy var newTeamAlert: UIAlertController = {
        let alert = UIAlertController(title: "Add team", message: nil, preferredStyle: .alert)
        alert.view.subviews[0].subviews[0].subviews[0].backgroundColor = UIColor(named: "alertColor")
        alert.addTextField(configurationHandler: { textField in
            textField.placeholder = "Team name"
            textField.autocapitalizationType = .sentences
        })
        alert.view.tintColor = UIColor(named: "accentColor")
        alert.addAction(UIAlertAction(title: "Cancel", style: .default))
        alert.addAction(UIAlertAction(title: "Create", style: .default, handler: { action in
            guard let text = alert.textFields?.first?.text, !text.isEmpty,
                !TBTeam.getAllNames().contains(text) else { return }
            let newTeam = TBTeam()
            newTeam.name = text
            newTeam.order = Int64(self.teams.count)
            CoreDataManager.sh.saveContext()
            self.teams.append(newTeam)
            self.tableView.insertRows(at: [IndexPath(row: Int(newTeam.order), section: 0)],
                                      with: .automatic)
        }))
        return alert
    }()
    
    //MARK: - methods
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        self.tableView.setEditing(editing, animated: animated)
        if editing {
            self.navigationItem.setRightBarButton(nil, animated: true)
        } else {
            self.navigationItem.setRightBarButton(self.addBarButtonItem, animated: true)
        }
    }
    
    @objc private func addButtonTapped() {
        self.newTeamAlert.textFields?.first?.text = ""
        self.present(self.newTeamAlert, animated: true)
    }
    
    private func loadData() {
        do {
            self.teams = try CoreDataManager.sh.container.viewContext.fetch(self.fetchRequest)
        } catch {
            print(error)
        }
        self.tableView.reloadData()
    }
    
    private func deleteData(at indexPath: IndexPath) {
        let team = self.teams[indexPath.row]
        self.teams.remove(at: indexPath.row)
        self.tableView.deleteRows(at: [indexPath], with: .automatic)
        CoreDataManager.sh.container.viewContext.delete(team)
        self.saveOrder()
    }
    
    private func saveOrder() {
        for (i, team) in self.teams.enumerated() {
            team.order = Int64(i)
        }
        CoreDataManager.sh.saveContext()
    }
    
    //MARK: - lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.setRightBarButton(self.addBarButtonItem, animated: false)
        self.navigationItem.setLeftBarButton(self.editButtonItem, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.loadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.setEditing(false, animated: false)
    }
}

    // MARK: - UITableView Delegate

extension TBMainViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 74
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.teams.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView
            .dequeueReusableCell(withIdentifier: Constants.ReuseIDs.titleDescriptionArrowCell,
                                 for: indexPath) as? TBTitleDescriptionArrowCell
            ?? TBTitleDescriptionArrowCell()
        cell.configure()
        
        let team = self.teams[indexPath.row]
        cell.titleLabel.text = team.name
        cell.descriptionLabel.text = "\(team.employees?.count ?? 0) " +
            (team.employees?.count == 1 ? "employee" : "employees")

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let team = self.teams[indexPath.row]
        if let controller = self.storyboard?
            .instantiateViewController(withIdentifier: Constants.StoryboardIDs.teamMainController)
            as? TBTeamMainViewController {
            controller.team = team
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        self.deleteData(at: indexPath)
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        self.teams.insert(self.teams.remove(at: sourceIndexPath.row), at: destinationIndexPath.row)
        self.saveOrder()
    }

}
