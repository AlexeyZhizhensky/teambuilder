//
//  TBSelectViewController.swift
//  TeamBuilder
//
//  Created by Alexey Zhizhensky on 12/25/19.
//  Copyright © 2019 Alexey Zhizhensky. All rights reserved.
//

import UIKit

class TBSelectViewController: UIViewController {
    
    //MARK: - variables
    
    let searchController = UISearchController(searchResultsController: nil)
    var data: [String] = []
    var filteredData: [String] = []
    var selectedItem = ""
    var itemDidSelect: ((String) -> Void)?
    private var isSearchBarEmpty: Bool {
        return self.searchController.searchBar.text?.isEmpty ?? true
    }
    private var isFiltering: Bool {
        return self.searchController.isActive && !self.isSearchBarEmpty
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    //MARK: - outlets
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            self.tableView.delegate = self
            self.tableView.dataSource = self
        }
    }
    
    //MARK: - methods
    
    private func filterContentForSearchText(_ searchText: String) {
        self.filteredData = self.data.filter { (item: String) -> Bool in
            return item.lowercased().contains(searchText.lowercased())
      }
      
      tableView.reloadData()
    }
    
    private func setupSearchBar() {
        self.searchController.searchBar.isTranslucent = false
        self.searchController.searchBar.barTintColor = UIColor(named: "accentColor")
        self.searchController.searchResultsUpdater = self
        self.searchController.obscuresBackgroundDuringPresentation = false
        self.searchController.hidesNavigationBarDuringPresentation = false
        self.navigationItem.searchController = self.searchController
        self.navigationItem.hidesSearchBarWhenScrolling = false
        self.definesPresentationContext = true
        let textField = self.searchController.searchBar.value(forKey: "searchField") as? UITextField
        (textField?.leftView as? UIImageView)?.tintColor = .white
        self.searchController.searchBar.tintColor = .white
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self])
            .defaultTextAttributes = [.foregroundColor: UIColor.white]
    }

    //MARK: - lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupSearchBar()
    }
}

extension TBSelectViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isFiltering {
            return self.filteredData.count
        } else {
            return self.data.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.ReuseIDs.titleCheckCell,
                                                 for: indexPath) as? TBTitleCheckCell
            ?? TBTitleCheckCell()
        if self.isFiltering {
            cell.titleLabel.text = self.filteredData[indexPath.row]
        } else {
            cell.titleLabel.text = self.data[indexPath.row]
        }
        if cell.titleLabel.text == self.selectedItem {
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        } else {
            tableView.deselectRow(at: indexPath, animated: false)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as? TBTitleCheckCell
        self.selectedItem = cell?.titleLabel.text ?? ""
        self.itemDidSelect?(self.selectedItem)
    }
}

extension TBSelectViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        self.filterContentForSearchText(searchBar.text ?? "")
    }
}
