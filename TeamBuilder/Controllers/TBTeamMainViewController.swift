//
//  TBTeamMainViewController.swift
//  TeamBuilder
//
//  Created by Alexey Zhizhensky on 1/31/20.
//  Copyright © 2020 Alexey Zhizhensky. All rights reserved.
//

import UIKit
import CoreData

class TBTeamMainViewController: UIViewController {
    
    //MARK: - outlets
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            self.tableView.delegate = self
            self.tableView.dataSource = self
        }
    }
    
    //MARK: - variables
    
    let fetchRequest: NSFetchRequest<TBEmployee> =
        CoreDataManager.sh.fetchRequest(entityName: "TBEmployee", keysForSort: ["position",
                                                                                "order",
                                                                                "secondName"])
    var team: TBTeam?
    var employees: [[TBEmployee]] = []
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK: - gui variables
    
    private lazy var moreBarButtonItem: UIBarButtonItem = {
        let item = UIBarButtonItem(image: UIImage(named: "moreIcon"),
                                   style: .plain,
                                   target: self,
                                   action: #selector(self.moreButtonTapped))
        return item
    }()
    
    private lazy var doneBarButtonItem: UIBarButtonItem = {
        let item = UIBarButtonItem(barButtonSystemItem: .done,
                                   target: self,
                                   action: #selector(self.doneButtonTapped))
        return item
    }()
    
    private lazy var moreAlert: UIAlertController = {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Edit", style: .default, handler: { action in
            self.tableView.setEditing(true, animated: true)
            self.navigationItem.rightBarButtonItem = self.doneBarButtonItem
        }))
        alert.addAction(UIAlertAction(title: "Add profile", style: .default, handler: { action in
            if let controller = self.storyboard?
                .instantiateViewController(withIdentifier: Constants.StoryboardIDs.editProfile)
                as? TBUserProfileEditViewController {
                controller.isNewEmployee = true
                controller.team = self.team
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        return alert
    }()
    
    //MARK: - methods
    
    @objc private func moreButtonTapped() {
        self.present(self.moreAlert, animated: true)
    }
    
    @objc private func doneButtonTapped() {
        self.tableView.setEditing(false, animated: true)
        self.navigationItem.rightBarButtonItem = self.moreBarButtonItem
    }
    
    private func loadData() {
        do {
            let employees = try CoreDataManager.sh.container.viewContext.fetch(self.fetchRequest)
            let dict = Dictionary(grouping: employees, by: { $0.position })
            self.employees = Array(dict.values)
        } catch {
            print(error)
        }
        self.tableView.reloadData()
    }
    
    private func deleteData(at indexPath: IndexPath) {
        let employee = self.employees[indexPath.section][indexPath.row]
        self.employees[indexPath.section].remove(at: indexPath.row)
        if self.employees[indexPath.section].isEmpty {
            self.employees.remove(at: indexPath.section)
            self.tableView.deleteSections(IndexSet(integer: indexPath.section), with: .automatic)
        } else {
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
            if let header = self.tableView.headerView(forSection: indexPath.section) {
                self.tableView(self.tableView,
                               willDisplayHeaderView: header,
                               forSection: indexPath.section)
            }
        }
        CoreDataManager.sh.container.viewContext.delete(employee)
        saveOrder()
    }
    
    private func saveOrder() {
        self.employees.forEach() { section in
            for (j, employee) in section.enumerated() {
                employee.order = Int64(j)
            }
        }
        CoreDataManager.sh.saveContext()
    }

    //MARK: - lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.register(TBTitleDescriptionHeader.self,
                                forHeaderFooterViewReuseIdentifier: TBTitleDescriptionHeader.reuseID)
        self.navigationItem.setRightBarButton(self.moreBarButtonItem, animated: false)
        
        if let team = self.team {
            self.fetchRequest.predicate = NSPredicate(format: "team == %@", team)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.loadData()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.tableView.setEditing(false, animated: false)
    }
}

    // MARK: - UITableView Delegate

extension TBTeamMainViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.employees.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.employees[section].count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return tableView.dequeueReusableHeaderFooterView(withIdentifier: TBTitleDescriptionHeader.reuseID)
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let view = view as? TBTitleDescriptionHeader,
            let title = self.employees[section].first?.position else { return }
        view.titleLabel.text = title
        let numberOfEmployees = self.employees[section].count
        view.descriptionLabel.text = "\(numberOfEmployees) " +
            (numberOfEmployees == 1 ? "employee" : "employees")
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(
            withIdentifier: Constants.ReuseIDs.iconTitleDescriptionCell,
            for: indexPath) as? TBIconTitleDescriptionCell ?? TBIconTitleDescriptionCell()
        cell.configure()
        
        let employee = self.employees[indexPath.section][indexPath.row]
        cell.titleLabel.text = employee.fullName
        cell.descriptionLabel.text = "\(employee.age) y.o., \(employee.rateHour)$ p/h"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let employee = self.employees[indexPath.section][indexPath.row]
        if let controller = self.storyboard?
            .instantiateViewController(withIdentifier: Constants.StoryboardIDs.profileController)
            as? TBUserProfileViewController {
            controller.employee = employee
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        self.deleteData(at: indexPath)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 46
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        if sourceIndexPath.section != destinationIndexPath.section {
            self.employees[sourceIndexPath.section][sourceIndexPath.row].position =
                self.employees[destinationIndexPath.section].first?.position
        }
        self.employees[destinationIndexPath.section]
            .insert(self.employees[sourceIndexPath.section].remove(at: sourceIndexPath.row),
                    at: destinationIndexPath.row)
        if self.employees[sourceIndexPath.section].isEmpty {
            self.employees.remove(at: sourceIndexPath.section)
            self.tableView.reloadData()
        } else {
            if let header = self.tableView.headerView(forSection: sourceIndexPath.section) {
                self.tableView(self.tableView,
                               willDisplayHeaderView: header,
                               forSection: sourceIndexPath.section)
            }
            if let header = self.tableView.headerView(forSection: destinationIndexPath.section) {
                self.tableView(self.tableView,
                               willDisplayHeaderView: header,
                               forSection: destinationIndexPath.section)
            }
        }
        self.saveOrder()
    }
}
