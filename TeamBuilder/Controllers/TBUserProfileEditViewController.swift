//
//  TBUserProfileEditViewController.swift
//  TeamBuilder
//
//  Created by Alexey Zhizhensky on 12/13/19.
//  Copyright © 2019 Alexey Zhizhensky. All rights reserved.
//

import UIKit

class TBUserProfileEditViewController: UIViewController {
    
    //MARK: - variables
    
    var employee: TBEmployee?
    var team: TBTeam?
    var isNewEmployee: Bool = false
    var dateFromPicker: Date?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK: - outlets
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var firstNameView: TBTitleDescriptionView!
    @IBOutlet weak var lastNameView: TBTitleDescriptionView!
    @IBOutlet weak var patronymicView: TBTitleDescriptionView!
    @IBOutlet weak var ageView: TBTitleDescriptionView! {
        didSet {
            self.ageView.descriptionTextField.inputView = self.datePicker
            self.ageView.descriptionTextField.inputAccessoryView = self.toolBar
            self.ageView.descriptionTextField.addTarget(self,
                                                        action: #selector(self.prepareDatePicker),
                                                        for: .editingDidBegin)
            self.ageView.descriptionTextField.tintColor = .clear
        }
    }
    @IBOutlet weak var locationView: TBTitleDescriptionArrowView! {
        didSet {
            self.locationView.addGestureRecognizer(
                UITapGestureRecognizer.init(target: self,
                                            action: #selector(self.locationDidTap)))
        }
    }
    @IBOutlet weak var positionView: TBTitleDescriptionArrowView! {
        didSet {
            self.positionView.addGestureRecognizer(
                UITapGestureRecognizer.init(target: self,
                                            action: #selector(self.positionDidTap)))
        }
    }
    @IBOutlet weak var phoneView: TBTitleDescriptionView! {
        didSet {
            self.phoneView.descriptionTextField.keyboardType = .phonePad
            self.phoneView.descriptionTextField.inputAccessoryView = self.toolBar
        }
    }
    @IBOutlet weak var emailView: TBTitleDescriptionView! {
        didSet {
            self.emailView.descriptionTextField.keyboardType = .emailAddress
        }
    }
    @IBOutlet weak var rateHourView: TBTitleDescriptionSliderView!
    @IBOutlet weak var rateCoefficientView: TBTitleSwitchView!
    @IBOutlet weak var experienceView: TBTitleDescriptionView! {
        didSet {
            self.experienceView.descriptionTextField.keyboardType = .numberPad
            self.experienceView.descriptionTextField.inputAccessoryView = self.toolBar
        }
    }
    @IBOutlet weak var teamView: TBTitleDescriptionArrowView! {
        didSet {
            self.teamView.addGestureRecognizer(
                UITapGestureRecognizer.init(target: self,
                                            action: #selector(self.teamDidTap)))
        }
    }
    @IBOutlet weak var aboutView: TBTitleTextView!
    @IBOutlet weak var deleteView: TBTitleView! {
        didSet {
            self.deleteView.addGestureRecognizer(
                UITapGestureRecognizer.init(target: self,
                                            action: #selector(self.deleteButtonTapped)))
        }
    }
    
    //MARK: - gui variables
    
    private lazy var datePicker: UIDatePicker = {
        let picker = UIDatePicker()
        picker.datePickerMode = .date
        picker.backgroundColor = UIColor.white
        picker.addTarget(self,
                         action: #selector(self.datePickerValueChanged),
                         for: .valueChanged)
        return picker
    }()
    
    private lazy var toolBar: UIToolbar = {
        let bar = UIToolbar()
        let flexibleBarButtonItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
                                                    target: self,
                                                    action: nil)
        let doneBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done,
                                                target: self,
                                                action: #selector(self.doneButtonTapped))
        bar.items = [flexibleBarButtonItem, doneBarButtonItem]
        bar.tintColor = UIColor(named: "accentColor")
        bar.sizeToFit()
        return bar
    }()
    
    private lazy var saveBarButtonItem: UIBarButtonItem = {
        let item = UIBarButtonItem(barButtonSystemItem: .save,
                                   target: self,
                                   action: #selector(self.saveButtonTapped))
        return item
    }()
    
    private lazy var deleteAlert: UIAlertController = {
        let alert = UIAlertController(title: "Are you sure?",
                                      message: "This user will be deleted forever!",
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .destructive, handler: { action in
            if let employee = self.employee {
                CoreDataManager.sh.container.viewContext.delete(employee)
                CoreDataManager.sh.saveContext()
                self.navigationController?.popViewController(animated: true)
                self.navigationController?.popViewController(animated: true)
            } else {
                self.navigationController?.popViewController(animated: true)
            }
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .default))
        return alert
    }()

    //MARK: - methods
    
    private func loadProfileToEdit() {
        self.teamView.descriptionText = self.team?.name
        if let employee = self.employee {
            self.firstNameView.descriptionText = employee.firstName
            self.lastNameView.descriptionText = employee.secondName
            self.patronymicView.descriptionText = employee.patronymic
            self.dateFromPicker = employee.birthDate
            self.ageView.descriptionText = String(employee.age)
            self.locationView.descriptionText = employee.location
            self.phoneView.descriptionText = employee.phone
            self.emailView.descriptionText = employee.email
            self.positionView.descriptionText = employee.position
            self.rateHourView.sliderValue = Float(employee.rateHour)
            self.rateCoefficientView.isSwitchOn = employee.rateCoefficient
            self.experienceView.descriptionText = String(employee.experience)
            self.aboutView.descriptionText = employee.about
        } else {
            self.firstNameView.descriptionText = ""
            self.lastNameView.descriptionText = ""
            self.patronymicView.descriptionText = ""
            self.dateFromPicker = Date()
            self.ageView.descriptionText = "0"
            self.locationView.descriptionText = ""
            self.phoneView.descriptionText = ""
            self.emailView.descriptionText = ""
            self.positionView.descriptionText = ""
            self.rateHourView.sliderValue = 0
            self.experienceView.descriptionText = ""
            self.aboutView.descriptionText = ""
        }
    }
    
    @objc private func positionDidTap() {
        if let controller = self.storyboard?
            .instantiateViewController(withIdentifier: Constants.StoryboardIDs.selectController)
            as? TBSelectViewController {
            controller.title = "Position"
            controller.data = Constants.positions
            controller.selectedItem = self.positionView.descriptionText ?? ""
            controller.itemDidSelect = { position in
                self.positionView.descriptionText = position
            }
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    @objc private func locationDidTap() {
        if let controller = self.storyboard?
            .instantiateViewController(withIdentifier: Constants.StoryboardIDs.selectController)
            as? TBSelectViewController {
            controller.title = "Location"
            controller.data = Constants.cities
            controller.selectedItem = self.locationView.descriptionText ?? ""
            controller.itemDidSelect = { location in
                self.locationView.descriptionText = location
            }
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    @objc private func teamDidTap() {
        if let controller = self.storyboard?
            .instantiateViewController(withIdentifier: Constants.StoryboardIDs.selectController)
            as? TBSelectViewController {
            controller.title = "Team"
            controller.data = TBTeam.getAllNames()
            controller.selectedItem = self.teamView.descriptionText ?? ""
            controller.itemDidSelect = { teamName in
                self.teamView.descriptionText = teamName
                self.team = TBTeam.getTeamBy(name: teamName)
            }
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    @objc private func saveButtonTapped() {
        if self.isNewEmployee {
            self.employee = TBEmployee()
            self.employee?.order = Int64.max
            self.isNewEmployee = false
        }
        guard let employee = self.employee,
            let firstName = self.firstNameView.descriptionText, !firstName.isEmpty,
            let secondName = self.lastNameView.descriptionText, !secondName.isEmpty,
            let team = self.team else { return }
        employee.firstName = firstName
        employee.secondName = secondName
        employee.patronymic = self.patronymicView.descriptionText ?? ""
        employee.birthDate = self.dateFromPicker ?? Date()
        employee.location = self.locationView.descriptionText ?? ""
        employee.phone = self.phoneView.descriptionText ?? ""
        employee.email = self.emailView.descriptionText ?? ""
        employee.position = self.positionView.descriptionText?
            .isEmpty ?? true ? "None" : self.positionView.descriptionText
        employee.experience = Int64(self.experienceView.descriptionText ?? "") ?? 0
        employee.team = team
        employee.rateHour = Int64(self.rateHourView.sliderValue.rounded())
        employee.rateCoefficient = self.rateCoefficientView.isSwitchOn
        employee.about = self.aboutView.descriptionText ?? ""
        
        CoreDataManager.sh.saveContext()

        self.navigationController?.popViewController(animated: true)
    }
    
    @objc private func deleteButtonTapped() {
        self.present(self.deleteAlert, animated: true)
    }
    
    @objc private func doneButtonTapped() {
        self.scrollView.endEditing(true)
    }
    
    @objc private func datePickerValueChanged() {
        self.dateFromPicker = self.datePicker.date
        self.ageView.descriptionText =
            String(Calendar.current.dateComponents([.year],
                                                   from: self.datePicker.date,
                                                   to: Date()).year ?? 0)
    }
    
    @objc private func prepareDatePicker() {
        self.datePicker.setDate(self.dateFromPicker ?? Date(), animated: false)
    }
    
    @objc private func adjustForKeyboard(notification: NSNotification) {
        if notification.name == UIResponder.keyboardWillHideNotification {
            self.scrollView.contentInset.bottom = 0
        } else if notification.name == UIResponder.keyboardWillShowNotification {
            guard let keyboardFrame =
                notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
                else { return }
            self.scrollView.contentInset.bottom =
                view.convert(keyboardFrame.cgRectValue, from: nil).size.height
        }
    }
    
    //MARK: - lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadProfileToEdit()
        
        self.navigationItem.setRightBarButton(self.saveBarButtonItem, animated: false)
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self,
                                       selector: #selector(self.adjustForKeyboard),
                                       name: UIResponder.keyboardWillShowNotification,
                                       object: nil)
        notificationCenter.addObserver(self,
                                       selector: #selector(self.adjustForKeyboard),
                                       name: UIResponder.keyboardWillHideNotification,
                                       object: nil)
    }
}
