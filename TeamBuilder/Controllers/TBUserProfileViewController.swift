//
//  ViewController.swift
//  TeamBuilder
//
//  Created by Alexey Zhizhensky on 12/6/19.
//  Copyright © 2019 Alexey Zhizhensky. All rights reserved.
//

import UIKit

class TBUserProfileViewController: UIViewController {

    //MARK: - variables
    
    var employee: TBEmployee?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13.0, *) {
            return .darkContent
        } else {
            return .default
        }
    }
    
    //MARK: - outlets
    
    @IBOutlet weak var userIconImageView: TBRoundedImageView!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var ageAndCityLabel: UILabel!
    @IBOutlet weak var teamLabel: UILabel!
    @IBOutlet weak var positionLabel: UILabel!
    @IBOutlet weak var experienceLabel: UILabel!
    @IBOutlet weak var rateHourLabel: UILabel!
    @IBOutlet weak var rateWeekLabel: UILabel!
    @IBOutlet weak var aboutUserTextView: UITextView!
    @IBOutlet weak var contactButton: TBRoundedButton! {
        didSet {
            self.contactButton.addTarget(self,
                                         action: #selector(self.contactButtonTapped),
                                         for: .touchUpInside)
        }
    }
    @IBOutlet weak var editBarButtonItem: UIBarButtonItem!

    //MARK: - actions
    
    @IBAction func editButtonTapped(_ sender: UIBarButtonItem) {
        if let controller = self.storyboard?
            .instantiateViewController(withIdentifier: Constants.StoryboardIDs.editProfile)
            as? TBUserProfileEditViewController {
            controller.employee = self.employee
            controller.team = self.employee?.team
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    @IBAction func shareButtonTapped(_ sender: Any) {
        let sharedInfo = "If you're interested, feel free to email me: \(self.employee?.email ?? "")"
        self.present(UIActivityViewController(activityItems: [sharedInfo],
                                              applicationActivities: nil),
                     animated: true, completion: nil)
    }
    
    //MARK: - methods
    
    func formattedNumber(number: String) -> String {
        let cleanPhoneNumber = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        var mask = ""
        if cleanPhoneNumber.count == 11 {
            mask = "+X XXX XXX-XX-XX"
        } else {
            mask = "+XXX XX XXX-XX-XX"
        }
        
        var result = ""
        var index = cleanPhoneNumber.startIndex
        for ch in mask where index < cleanPhoneNumber.endIndex {
            if ch == "X" {
                result.append(cleanPhoneNumber[index])
                index = cleanPhoneNumber.index(after: index)
            } else {
                result.append(ch)
            }
        }
        return result
    }
    
    @objc func contactButtonTapped() {
        let contactAlert = UIAlertController(title: nil,
                                      message: nil,
                                      preferredStyle: .actionSheet)
        let actionPhone = UIAlertAction(title: "Phone: " +
            self.formattedNumber(number: self.employee?.phone ?? ""),
                                        style: .default,
                                        handler: { action in
            guard let url = URL(string: "tel://\(self.employee?.phone ?? "")") else { return }
            UIApplication.shared.open(url)
        })
        if self.employee?.phone?.isEmpty ?? true {
            actionPhone.isEnabled = false
        }
        let actionEmail = UIAlertAction(title: "Email", style: .default, handler: { action in
            guard let url = URL(string: "mailto:\(self.employee?.email ?? "")") else { return }
            UIApplication.shared.open(url)
        })
        if self.employee?.email?.isEmpty ?? true {
            actionEmail.isEnabled = false
        }
        contactAlert.addAction(actionPhone)
        contactAlert.addAction(actionEmail)
        contactAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        
        self.present(contactAlert, animated: true)
    }

    private func loadProfile() {
        if let employee = self.employee {
            self.fullNameLabel.text = employee.fullName
            self.ageAndCityLabel.text = "\(employee.age) years, \(employee.location ?? "")"
            self.teamLabel.text = employee.team.name
            self.positionLabel.text = employee.position
            self.experienceLabel.text = "\(employee.experience) " +
                (employee.experience == 1 ? "year" : "years")
            self.rateHourLabel.text = "\(employee.rateHour)$"
            self.rateWeekLabel.text = "\(employee.rateWeek)$"
            self.aboutUserTextView.text = employee.about
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }

    //MARK: - lifecycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.loadProfile()
        
        let navBar = self.navigationController?.navigationBar
        navBar?.barTintColor = .white
        navBar?.tintColor = UIColor(named: "accentColor")
        
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.backgroundColor = .white
            appearance.shadowColor = .clear
            navBar?.standardAppearance = appearance
            navBar?.scrollEdgeAppearance = appearance
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        let navBar = self.navigationController?.navigationBar
        navBar?.barTintColor = UIColor(named: "accentColor")
        navBar?.tintColor = .white
        
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.backgroundColor = UIColor(named: "accentColor")
            appearance.titleTextAttributes = [.foregroundColor: UIColor.white]
            appearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
            navBar?.standardAppearance = appearance
            navBar?.scrollEdgeAppearance = appearance
        }
    }
}
