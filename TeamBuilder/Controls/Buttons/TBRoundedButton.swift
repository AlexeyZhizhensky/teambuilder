//
//  TBRoundedButton.swift
//  TeamBuilder
//
//  Created by Alexey Zhizhensky on 12/6/19.
//  Copyright © 2019 Alexey Zhizhensky. All rights reserved.
//

import UIKit

@IBDesignable
class TBRoundedButton: UIButton {
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return self.layer.cornerRadius
        }
        set {
            self.layer.cornerRadius = newValue
        }
    }
    
    override func prepareForInterfaceBuilder() {
        self.layer.cornerRadius = self.cornerRadius
    }
}

