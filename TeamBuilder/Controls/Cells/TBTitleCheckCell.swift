//
//  TBTitleCheckCell.swift
//  TeamBuilder
//
//  Created by Alexey Zhizhensky on 12/25/19.
//  Copyright © 2019 Alexey Zhizhensky. All rights reserved.
//

import UIKit

class TBTitleCheckCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var checkImageView: UIImageView!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        self.checkImageView.isHidden = !selected
    }
}
