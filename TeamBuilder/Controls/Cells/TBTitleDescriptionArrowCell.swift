//
//  TBTitleDescriptionArrowCell.swift
//  TeamBuilder
//
//  Created by Alexey Zhizhensky on 1/31/20.
//  Copyright © 2020 Alexey Zhizhensky. All rights reserved.
//

import UIKit

class TBTitleDescriptionArrowCell: UITableViewCell {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    func configure() {
        self.containerView.layer.borderColor = UIColor(named: "cellBorderColor")?.cgColor
        self.containerView.layer.borderWidth = 0.2
        self.containerView.layer.shadowOpacity = 1
        self.containerView.layer.cornerRadius = 5
        self.containerView.layer.shadowColor = UIColor(named: "shadowColor")?.cgColor
        self.containerView.layer.shadowRadius = 5
        self.containerView.layer.shadowOffset = CGSize(width: 0, height: 4)
    }
}
