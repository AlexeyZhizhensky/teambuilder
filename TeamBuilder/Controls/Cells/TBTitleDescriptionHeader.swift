//
//  TBTitleDescriptionHeader.swift
//  TeamBuilder
//
//  Created by Alexey Zhizhensky on 2/1/20.
//  Copyright © 2020 Alexey Zhizhensky. All rights reserved.
//

import UIKit

class TBTitleDescriptionHeader: UITableViewHeaderFooterView {
    static let reuseID = "titleDescriptionHeader"
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        self.loadFromNib()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.loadFromNib()
    }
}
