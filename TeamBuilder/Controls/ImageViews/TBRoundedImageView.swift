//
//  TBRoundedImageView.swift
//  TeamBuilder
//
//  Created by Alexey Zhizhensky on 12/6/19.
//  Copyright © 2019 Alexey Zhizhensky. All rights reserved.
//

import UIKit

@IBDesignable
class TBRoundedImageView: UIImageView {
    @IBInspectable
    var isCircle: Bool = true {
        didSet {
            if self.isCircle {
                self.layer.cornerRadius = self.frame.size.height / 2
            } else {
                self.layer.cornerRadius = 0
            }
        }
    }
}
