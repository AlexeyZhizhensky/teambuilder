//
//  TBTitleDescriptionArrowView.swift
//  TeamBuilder
//
//  Created by Alexey Zhizhensky on 12/8/19.
//  Copyright © 2019 Alexey Zhizhensky. All rights reserved.
//

import UIKit

@IBDesignable
class TBTitleDescriptionArrowView: UIView {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var arrowImageView: UIImageView!
    
    @IBInspectable
    var title: String? {
        get {
            return self.titleLabel.text
        }
        set {
            self.titleLabel.text = newValue
        }
    }
    
    @IBInspectable
    var descriptionText: String? {
        get {
            return self.descriptionLabel.text
        }
        set {
            self.descriptionLabel.text = newValue
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadFromNib()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.loadFromNib()
    }
}
