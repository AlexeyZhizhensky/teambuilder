//
//  TBTitleDescriptionSliderView.swift
//  TeamBuilder
//
//  Created by Alexey Zhizhensky on 12/8/19.
//  Copyright © 2019 Alexey Zhizhensky. All rights reserved.
//

import UIKit

@IBDesignable
class TBTitleDescriptionSliderView: UIView {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var horizontalSlider: UISlider!
    @IBOutlet weak var minimumSliderValueLabel: UILabel!
    @IBOutlet weak var maximumSliderValueLabel: UILabel!
    
    @IBInspectable
    var title: String? {
        get {
            return self.titleLabel.text
        }
        set {
            self.titleLabel.text = newValue
        }
    }

    @IBInspectable
    var minimumSliderValue: Float {
        get {
            return self.horizontalSlider.minimumValue
        }
        set {
            self.horizontalSlider.minimumValue = newValue
            self.minimumSliderValueLabel.text = "\(Int(newValue.rounded()))$"
        }
    }
    
    @IBInspectable
    var maximumSliderValue: Float {
        get {
            return self.horizontalSlider.maximumValue
        }
        set {
            self.horizontalSlider.maximumValue = newValue
            self.maximumSliderValueLabel.text = "\(Int(newValue.rounded()))$"
        }
    }

    @IBInspectable
    var sliderValue: Float {
        get {
            self.horizontalSlider.value
        }
        set {
            self.horizontalSlider.value = newValue
            self.descriptionLabel.text = "\(Int(newValue.rounded()))$"
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadFromNib()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.loadFromNib()
    }
    
    @IBAction func sliderValueChanged(_ sender: UISlider) {
        self.descriptionLabel.text = "\(Int(self.horizontalSlider.value.rounded()))$"
    }
}
