//
//  TBTitleDescriptionView.swift
//  TeamBuilder
//
//  Created by Alexey Zhizhensky on 12/8/19.
//  Copyright © 2019 Alexey Zhizhensky. All rights reserved.
//

import UIKit

@IBDesignable
class TBTitleDescriptionView: UIView, UITextFieldDelegate {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionTextField: UITextField!
    
    @IBInspectable
    var title: String? {
        get {
            return self.titleLabel.text
        }
        set {
            self.titleLabel.text = newValue
        }
    }
    
    @IBInspectable
    var descriptionText: String? {
        get {
            return self.descriptionTextField.text
        }
        set {
            self.descriptionTextField.text = newValue
        }
    }
    
    @IBInspectable
    var descriptionHint: String? {
        get {
            return self.descriptionTextField.placeholder
        }
        set {
            self.descriptionTextField.placeholder = newValue
        }
    }
    
    @IBInspectable
    var onlyDigits: Bool = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadFromNib()
        self.descriptionTextField.delegate = self
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.loadFromNib()
        self.descriptionTextField.delegate = self
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard self.onlyDigits else { return true }
        if string.rangeOfCharacter(from: CharacterSet(charactersIn: "+0123456789*#")) != nil ||
            string.isEmpty {
            return true
        } else {
            return false
        }
    }
    
    @IBAction func editDidEnd(_ sender: UITextField) {
        sender.resignFirstResponder()
    }
}
