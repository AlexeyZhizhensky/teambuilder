//
//  TBTitleSwitchView.swift
//  TeamBuilder
//
//  Created by Alexey Zhizhensky on 12/8/19.
//  Copyright © 2019 Alexey Zhizhensky. All rights reserved.
//

import UIKit

@IBDesignable
class TBTitleSwitchView: UIView {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var switcher: UISwitch!
    
    @IBInspectable
    var title: String? {
        get {
            return self.titleLabel.text
        }
        set {
            self.titleLabel.text = newValue
        }
    }
    
    @IBInspectable
    var isSwitchOn: Bool {
        get {
            return self.switcher.isOn
        }
        set {
            self.switcher.isOn = newValue
        }
    }

    @IBInspectable
    var onTintColor: UIColor? {
        get {
            return self.switcher?.onTintColor
        }
        set {
            self.switcher.onTintColor = newValue
        }
    }
    
    @IBInspectable
    var offTintColor: UIColor? {
        get {
            return self.switcher?.backgroundColor
        }
        set {
            self.switcher.layer.cornerRadius = self.switcher.frame.size.height / 2
            self.switcher.backgroundColor = newValue
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadFromNib()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.loadFromNib()
    }
}
