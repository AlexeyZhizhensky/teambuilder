//
//  TBTitleView.swift
//  TeamBuilder
//
//  Created by Alexey Zhizhensky on 12/8/19.
//  Copyright © 2019 Alexey Zhizhensky. All rights reserved.
//

import UIKit

@IBDesignable
class TBTitleView: UIView {
    @IBOutlet weak var textLabel: UILabel!
    
    @IBInspectable
    var text: String? {
        get {
            return self.textLabel.text
        }
        set {
            self.textLabel.text = newValue
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadFromNib()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.loadFromNib()
    }
}
