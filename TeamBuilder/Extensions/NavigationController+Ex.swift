//
//  NavigationController+Ex.swift
//  TeamBuilder
//
//  Created by Alexey Zhizhensky on 2/8/20.
//  Copyright © 2020 Alexey Zhizhensky. All rights reserved.
//

import UIKit

extension UINavigationController {
    override open var preferredStatusBarStyle: UIStatusBarStyle {
        if let topVC = viewControllers.last {
            return topVC.preferredStatusBarStyle
        }
        return .lightContent
    }
}
