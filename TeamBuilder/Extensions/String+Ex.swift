//
//  String+Ex.swift
//  TeamBuilder
//
//  Created by Alexey Zhizhensky on 12/25/19.
//  Copyright © 2019 Alexey Zhizhensky. All rights reserved.
//

import Foundation

extension String {
    func capitalizedFirstLetter() -> String {
        return self.prefix(1).uppercased() + self.dropFirst()
    }
}
