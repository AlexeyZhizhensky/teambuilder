//
//  TBEmployee+CoreDataClass.swift
//  TeamBuilder
//
//  Created by Alexey Zhizhensky on 2/7/20.
//  Copyright © 2020 Alexey Zhizhensky. All rights reserved.
//
//

import Foundation
import CoreData


public class TBEmployee: NSManagedObject {
    var fullName: String {
        return [self.secondName, self.firstName, self.patronymic ?? ""].joined(separator: " ")
    }
    
    var age: Int {
        return Calendar.current.dateComponents([.year],
                                               from: self.birthDate ?? Date(),
                                               to: Date()).year ?? 0
    }
    
    var rateWeek: Int {
        return Int((Double(self.rateHour) * 40 * self.getCoefficient()).rounded(.up))
    }
    
    private func getCoefficient() -> Double {
        guard self.rateCoefficient else { return 1 }
        if self.experience >= 3 && self.experience <= 5 { return 1.5 }
        else if self.experience > 5 { return 2 }
        else { return 1 }
    }
    
    convenience init() {
        self.init(context: CoreDataManager.sh.container.viewContext)
    }

}
