//
//  TBEmployee+CoreDataProperties.swift
//  TeamBuilder
//
//  Created by Alexey Zhizhensky on 2/8/20.
//  Copyright © 2020 Alexey Zhizhensky. All rights reserved.
//
//

import Foundation
import CoreData


extension TBEmployee {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<TBEmployee> {
        return NSFetchRequest<TBEmployee>(entityName: "TBEmployee")
    }

    @NSManaged public var about: String?
    @NSManaged public var birthDate: Date?
    @NSManaged public var email: String?
    @NSManaged public var experience: Int64
    @NSManaged public var firstName: String
    @NSManaged public var location: String?
    @NSManaged public var patronymic: String?
    @NSManaged public var phone: String?
    @NSManaged public var position: String?
    @NSManaged public var rateCoefficient: Bool
    @NSManaged public var rateHour: Int64
    @NSManaged public var secondName: String
    @NSManaged public var order: Int64
    @NSManaged public var team: TBTeam

}
