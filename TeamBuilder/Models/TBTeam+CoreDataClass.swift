//
//  TBTeam+CoreDataClass.swift
//  TeamBuilder
//
//  Created by Alexey Zhizhensky on 2/7/20.
//  Copyright © 2020 Alexey Zhizhensky. All rights reserved.
//
//

import Foundation
import CoreData


public class TBTeam: NSManagedObject {
    convenience init() {
        self.init(context: CoreDataManager.sh.container.viewContext)
    }
    
    static func getAllNames() -> [String] {
        var data: [String] = []
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "TBTeam")
        do {
            let objects = try CoreDataManager.sh.container.viewContext.fetch(request)
            objects.forEach() { object in
                guard let team = object as? TBTeam else { return }
                data.append(team.name)
            }
        } catch {
            fatalError("Failed to fetch employees: \(error)")
        }
        return data
    }
    
    static func getTeamBy(name: String) -> TBTeam? {
        let predicate = NSPredicate(format: "name == %@", name)
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "TBTeam")
        request.predicate = predicate
        do {
            let objects = try CoreDataManager.sh.container.viewContext.fetch(request)
            return objects.first as? TBTeam
        } catch {
            fatalError("Failed to fetch employees: \(error)")
        }
    }
}
