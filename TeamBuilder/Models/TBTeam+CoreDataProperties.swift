//
//  TBTeam+CoreDataProperties.swift
//  TeamBuilder
//
//  Created by Alexey Zhizhensky on 2/8/20.
//  Copyright © 2020 Alexey Zhizhensky. All rights reserved.
//
//

import Foundation
import CoreData


extension TBTeam {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<TBTeam> {
        return NSFetchRequest<TBTeam>(entityName: "TBTeam")
    }

    @NSManaged public var name: String
    @NSManaged public var order: Int64
    @NSManaged public var employees: NSSet?

}
