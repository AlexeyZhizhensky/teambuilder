//
//  Constants.swift
//  TeamBuilder
//
//  Created by Alexey Zhizhensky on 12/15/19.
//  Copyright © 2019 Alexey Zhizhensky. All rights reserved.
//

import Foundation

struct Constants {
    struct StoryboardIDs {
        static let profileController = "profileController"
        static let editProfile = "editProfileController"
        static let selectController = "selectController"
        static let mainController = "mainController"
        static let teamMainController = "teamMainController"
    }
    struct ReuseIDs {
        static let titleCheckCell = "titleCheckCell"
        static let titleDescriptionArrowCell = "titleDescriptionArrowCell"
        static let iconTitleDescriptionCell = "iconTitleDescriptionCell"
    }
    static let cities: [String] = ["Abingdon", "Amsterdam", "Arkhangelsk", "Atlanta", "Athens",
                                   "Baden", "Basel", "Bangalore", "Bender", "Berlin", "Baranovichy",
                                   "Beloozërsk", "Berëza", "Brest", "David-Gorodok", "Drogichin",
                                   "Gantsevichi", "Ivanovo", "Ivatsevichi", "Kamenets", "Kobrin",
                                   "Kossovo", "Luninets", "Lyakhovichi", "Malorita", "Mikashevichi",
                                   "Pinsk", "Pruzhany", "Stolin", "Vysokoye", "Zhabinka", "Baran’",
                                   "Braslav", "Chashniki", "Disna", "Dokshitsy", "Dubrovno",
                                   "Glubokoye", "Gorodok", "Lepel’", "Miory", "Novolukoml’",
                                   "Novopolotsk", "Orsha", "Polotsk", "Postavy", "Senno", "Tolochin",
                                   "Verkhnedvinsk", "Vitebsk", "Buda-Koshelëvo", "Chechersk",
                                   "Dobrush", "Gomel’", "Kalinkovichi", "Khoyniki", "Mozyr’",
                                   "Narovlya", "Petrikov", "Rechitsa", "Rogachëv", "Svetlogorsk",
                                   "Turov", "Vasilevichi", "Vetka", "Yel’sk", "Zhinkovichi",
                                   "Zhlobin", "Berëzovka", "Dyatlovo", "Grodno", "Iv’ye", "Lida",
                                   "Mosty", "Novogrudok", "Oshmyany", "Shchuchin", "Skidel’",
                                   "Slonim", "Smorgon", "Svisloch", "Volkovysk", "Berezino",
                                   "Borisov", "Cherven’", "Dzerzhinsk", "Fanipol’", "Kletsk",
                                   "Kopyl’", "Krupki", "Logoysk", "Lyuban’", "Mar’ina Gorka",
                                   "Minsk", "Molodechno", "Myadel’", "Nesvizh", "Slutsk",
                                   "Smolevichi", "Soligorsk", "Staryye Dorogi", "Stolbtsy", "Uzda",
                                   "Vileyka", "Volozhin", "Zaslavl’", "Zhodino", "Bobruysk",
                                   "Bykhov", "Chausy", "Cherikov", "Gorki", "Kirovsk", "Klichev",
                                   "Klimovichi", "Kostyukovichi", "Krichev", "Mogilev",
                                   "Mstislavl’", "Osipovichi", "Shklov",
                                   "Slavgorod", "Moscow"].sorted()
    static var positions: [String] = ["Developer",
                                      "Designer",
                                      "Product Manager",
                                      "Business Analyst"].sorted()
}
