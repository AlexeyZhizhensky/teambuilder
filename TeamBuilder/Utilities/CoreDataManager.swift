//
//  CoreDataManager.swift
//  TeamBuilder
//
//  Created by Alexey Zhizhensky on 2/7/20.
//  Copyright © 2020 Alexey Zhizhensky. All rights reserved.
//

import CoreData
import Foundation

class CoreDataManager {
    
    static let sh = CoreDataManager()
    
    private init() {}

    func fetchRequest<T: NSFetchRequestResult>(entityName: String,
                                               keysForSort: [String]) -> NSFetchRequest<T> {
        let request = NSFetchRequest<T>(entityName: entityName)
        request.sortDescriptors = []
        keysForSort.forEach() { key in
            request.sortDescriptors?.append(NSSortDescriptor(key: key, ascending: true))
        }
        return request
    }
    
    // MARK: - Core Data stack
    
    lazy var container: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "coreDataModel")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = container.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}
